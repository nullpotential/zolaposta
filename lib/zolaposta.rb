require 'toml-rb'
require 'date'
require 'colorize'

def generate_frontmatter(title, description, tags)
  frontmatter = {
    'date' => Date.today.strftime('%Y-%m-%d')
  }

  frontmatter['title'] = title unless title.empty?

  frontmatter['description'] = description unless description.empty?

  frontmatter['taxonomies'] = { 'tags' => tags } unless tags.empty?

  TomlRB.dump(frontmatter)
end

def get_user_input(prompt)
  print prompt.colorize(color: :blue, mode: :bold)
  gets.chomp
end

def get_tags_from_user()
  tags = []

  loop do
    tag = get_user_input('Enter a tag (press enter to add another tag, or leave blank if done): ')
    break if tag.empty?

    tags << tag
  end

  tags
end

def cancel_operation()
  puts 'Operation cancelled. Exiting...'.colorize(:red)
  exit
end

puts "\n--------------------------------".colorize(:green)
puts "Welcome to zolaposta!".colorize(:green)
puts "This app allows you to generate frontmatter for your blog posts.".colorize(:green)
puts "You can type 'cancel' at any time to quit.".colorize(:green)
puts "All fields are optional; simply press enter to skip a field.".colorize(:green)
puts "--------------------------------\n".colorize(:green)

title = get_user_input('Enter post title: ')
cancel_operation if title.downcase == 'cancel'

filename = if title.empty?
             "#{Date.today.strftime('%Y-%m-%d')}.md"
           else
             "#{Date.today.strftime('%Y-%m-%d')}-#{title.downcase.gsub(' ', '-')}.md"
           end

if File.exist?(filename)
  puts "The file #{filename} already exists.".colorize(:red)
  action = get_user_input("Choose an action: 'overwrite', 'rename', or 'cancel': ")

  case action.downcase
  when 'overwrite'
    puts "Overwriting the existing file..."
  when 'rename'
    timestamp = Time.now.strftime('%Y-%m-%d-%H-%M-%S')
    filename = "#{filename.gsub('.md', '')}-#{timestamp}.md"
    puts "File renamed to #{filename}."
  when 'cancel'
    cancel_operation
  else
    puts "Invalid action. Cancelling operation..."
    cancel_operation
  end
end

description = get_user_input('Enter description: ')
cancel_operation if description.downcase == 'cancel'

tags = get_tags_from_user()
cancel_operation if tags.include?('cancel')

frontmatter = generate_frontmatter(title, description, tags)

File.open(filename, 'w') do |file|
  file.puts "+++\n"
  file.puts frontmatter
  file.puts "+++\n"
end

puts "Frontmatter generated and saved to #{filename}.".colorize(:yellow)
