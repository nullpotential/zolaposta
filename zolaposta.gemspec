# frozen_string_literal: true

require_relative "lib/zolaposta/version"

Gem::Specification.new do |spec|
  spec.name = "zolaposta"
  spec.version = '0.1.1'
  spec.authors = ["nullpotential"]
  spec.email = ["nupo@08182838.xyz"]

  spec.summary = "A frontmatter generator for Zola blog posts."
  spec.description = "This gem provides a command-line interface to generate frontmatter for Zola blog posts. Zola is a static site generator build with rust."
  spec.homepage = "https://codeberg.org/nullpotential/zolaposta"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://codeberg.org/nullpotential/zolaposta"
  spec.metadata["changelog_uri"] = "https://codeberg.org/nullpotential/zolaposta"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git appveyor Gemfile])
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"
  spec.add_dependency 'toml-rb'
  spec.add_dependency 'date'
  spec.add_dependency 'colorize'
  spec.add_runtime_dependency 'thor'
    
  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
